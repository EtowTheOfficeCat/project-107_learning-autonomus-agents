using UnityEditor;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    public bool ShowDebug;
    public float Mass = 1f;
    public Vector3 Velocity;
    public float MaxForce = 1f;
    public float MaxSpeed = 1f;
    public SteeringBehaviour[] steeringBehaviours;
    private Vector3 acceleration;
    private float forceAccum;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        steeringBehaviours = GetComponents<SteeringBehaviour>();
    }

    public void ApplyForce(Vector3 force, float weight, out bool forceExhausted)
    {
        forceAccum += force.magnitude * weight;
        force = (Time.fixedDeltaTime / Mass) * force * weight;
        float forceLeft = MaxForce - forceAccum;
        if (forceAccum > MaxForce)
        {
            float difference = forceAccum - MaxForce;
            force = Vector3.ClampMagnitude(force, force.magnitude - difference);
            acceleration += force;
            forceExhausted = true;
            return;
        }
        else
        {
            acceleration += force;
            forceExhausted = false;
        }
    }

    public void UpdateVehicle()
    {
        forceAccum = 0f;
        Velocity += acceleration;
        Velocity = Vector3.ClampMagnitude(Velocity, MaxSpeed);
        transform.position += Velocity * Time.deltaTime;
        //rb.velocity = Velocity;
        //rb.AddForce(Velocity);
        acceleration = Vector3.zero;
    }
}
