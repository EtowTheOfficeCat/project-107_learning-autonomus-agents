using UnityEngine;

public class ObstacleAvoidBehaviour : SteeringBehaviour
{
    public Obstacle[] Obstacles;
    public float maxAhead = 5f;

    public override Vector3 Steer()
    {
        Vector3 antenna = vehicle.Velocity * maxAhead;
        //Debug.DrawRay(transform.position, antenna, Color.blue);
        Obstacle threat = null;

        Vector3 nearestPointToObstacle = Vector3.zero;
        for (int i = 0; i < Obstacles.Length; i++)
        {
            var o = Obstacles[i];
            var pos = transform.position;
            var toObstacle = o.Position - pos;
            float percOnAntenna = (Vector3.Dot(antenna, toObstacle)) / antenna.sqrMagnitude;
            if (percOnAntenna < 0f || percOnAntenna > 1f)
            {
                continue;
            }
            var nearestOnAntenna = pos + antenna * percOnAntenna;
            nearestPointToObstacle = o.Position - nearestOnAntenna;
            var sqrRadius = o.Radius * o.Radius;
            if (sqrRadius > nearestPointToObstacle.sqrMagnitude)
            {
                //threat gefunden
                if (threat == null || toObstacle.sqrMagnitude < (threat.Position - pos).sqrMagnitude)
                {
                    threat = o;
                }
            }
        }

        if (threat != null)
        {
            var desired = -nearestPointToObstacle * (nearestPointToObstacle.magnitude + threat.Radius * 2f);
            desired = desired * vehicle.MaxSpeed;
            var steer = desired - vehicle.Velocity;
            steer = Vector3.ClampMagnitude(steer, vehicle.MaxForce);
            return steer;
        }
        else
        {
            return Vector3.zero;
        }
    }
}
