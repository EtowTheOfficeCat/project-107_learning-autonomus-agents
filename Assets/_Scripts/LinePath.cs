﻿using UnityEngine;

public class LinePath : MonoBehaviour
{
    public float Radius = 2f;
    public LineSegment[] LineSegments;

    void Start()
    {
        Transform[] children = new Transform[transform.childCount];
        LineSegments = new LineSegment[transform.childCount];
        int idx = 0;
        foreach (Transform child in transform)
        {
            children[idx] = child;
            idx++;
        }
        for (int i = 0, j = 0; i < children.Length; i++)
        {
            j = (j + 1) % children.Length;
            LineSegments[i] = new LineSegment(children[i].position, children[j].position, Radius);
        }
    }

    private void Update()
    {
        foreach (LineSegment lineSegment in LineSegments)
        {
            Debug.DrawLine(lineSegment.Start, lineSegment.End, Color.magenta);
        }
    }
}
