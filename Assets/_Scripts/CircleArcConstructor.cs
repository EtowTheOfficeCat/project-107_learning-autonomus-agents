using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CircleArcConstructor : MonoBehaviour
{
    //https://math.stackexchange.com/questions/1781438/finding-the-center-of-a-circle-given-two-points-and-a-radius-algebraically

    public float radiusPlus = 2f;
    public int numSegments = 10;

    public Transform p1Transf;
    public Transform p2Transf;
    Vector3 p1;
    Vector3 p2;
    Vector3 center;
    float angle;

    void Start()
    {
    }

    void Update()
    {
        p1 = p1Transf.position;
        p2 = p2Transf.position;
        //Vector3 middle = (p1 + p2) / 2f;
        Debug.DrawLine(p1, p2, Color.yellow);
        float xa = (p2.x - p1.x) * 0.5f;
        float ya = (p2.z - p1.z) * 0.5f;
        Vector3 a = new Vector3(xa, 0f, ya);
        Vector3 middle = p1 + a;
        Debug.DrawLine(p1, middle, Color.red);
        float radius = a.magnitude + radiusPlus; // radius must be bigger than the distance between points
        float rSqr = radius * radius;
        float bSqrMag = rSqr - a.sqrMagnitude;
        float bMag = Mathf.Sqrt(bSqrMag);
        float x3 = middle.x + ((bMag * ya) / a.magnitude);
        float y3 = middle.z - ((bMag * xa) / a.magnitude);
        center = new Vector3(x3, 0f, y3);
        Debug.DrawLine(p1, center, Color.green);
        Debug.DrawLine(p2, center, Color.green);
        Vector3 p1ToCenter = center - p1;
        Vector3 p2ToCenter = center - p2;
        Vector3 centerToP1 = -p1ToCenter;
        angle = Vector3.Angle(p1ToCenter, p2ToCenter);
        float step = angle / numSegments;
        for (int i = 0; i < numSegments; i++)
        {
            Quaternion rot = Quaternion.Euler(new Vector3(0f, step * i, 0f));
            Vector3 dir = rot * centerToP1;
            Debug.DrawRay(center, dir, Color.cyan);
        }
    }

    private void OnDrawGizmos()
    {
        //Gizmos.DrawSphere(center, 1f);
        Handles.Label(center - Vector3.left, angle.ToString());
    }
}
